
#include <thrust/device_vector.h>
#include <cublas_v2.h>
#include <iostream>
#include <thrust/random.h>
#include <math.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <random>
#include <iostream>
#include <thrust/random/linear_congruential_engine.h>
#include <thrust/random/uniform_real_distribution.h>
#include "TimingCPU.h"
#include "TimingGPU.cuh"
#include <thrust/transform.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/fill.h>
#include <vector>

#include <time.h>
#include <sys/time.h>

unsigned long long dtime_usec(unsigned long long prev){
#define USECPSEC 1000000ULL
  timeval tv1;
  gettimeofday(&tv1,0);
  return ((tv1.tv_sec * USECPSEC)+tv1.tv_usec) - prev;
}

thrust::minstd_rand rng;
thrust::uniform_real_distribution<float> dist(-7,13);

struct tanh_functor
{
  __host__ __device__
  double operator()(const double& x) const
  {
	double pez = exp(x);
	double nez = exp(-x);

	return (pez - nez) / (pez + nez);
  }
};


struct GenRand
{
	__device__
	double operator () (int idx)
	{
		thrust::default_random_engine randEng;
		thrust::uniform_real_distribution<double> uniDist;
		randEng.discard(idx);
		return uniDist(randEng);
	}
};


struct ElementWiseMult
{
	  __host__ __device__
	  double operator()(const double& x, const double& y) const
	  {
		return x * y;
	  }
};

void checkNonLinearElementWise()
{
	thrust::device_vector<double> inputVector;
	inputVector.resize(100);

	thrust::device_vector<double> outputVector;
	outputVector.resize(100);

	thrust::device_vector<double> outputVector1;
	outputVector1.resize(100);

	thrust::fill(outputVector.begin(), outputVector.end(), 0.0);

	thrust::fill(outputVector1.begin(), outputVector1.end(), 0.0);

	thrust::transform(thrust::make_counting_iterator(0), thrust::make_counting_iterator(100), inputVector.begin(), GenRand());

	std::cout << "Size of inputVector " << inputVector.size() << std::endl;

	std::cout << "Size of outputVector " << outputVector.size() << std::endl;

	std::cout << "Size of outputVector1 " << outputVector1.size() << std::endl;

	clock_t start = clock();

	thrust::transform(inputVector.begin(), inputVector.end(), outputVector.begin(), tanh_functor());

	clock_t stop = clock();

	double elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
	std::cout << "Time elapsed in s: " << elapsed << std::endl;

	start = clock();

	int i = 0;
	i = 0;
	for(thrust::device_vector<double>::iterator begin = inputVector.begin(); begin != inputVector.end(); begin++)
	{
		double pez = exp(*begin);
		double nez = exp(-1.0 * (*begin));

		outputVector1[i] = (pez - nez) / (pez + nez);
		i++;
	}

	stop = clock();

	elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
	std::cout << "Time elapsed in s: " << elapsed << std::endl;

	for(i = 0 ; i < outputVector.size(); i++)
	{
		std::cout << outputVector[i] << "\t" << outputVector1[i] << std::endl;
	}

	start = clock();

	i = 0;
	for(i = 0; i < inputVector.size(); i++)
	{
		double pez = exp(inputVector[i]);
		double nez = exp(-1.0 * inputVector[i]);

		outputVector1[i] = (pez - nez) / (pez + nez);
		i++;
	}

	stop = clock();

	elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
	std::cout << "Time elapsed in s: " << elapsed << std::endl;

	for(i = 0 ; i < outputVector.size(); i++)
	{
		std::cout << outputVector[i] << "\t" << outputVector1[i] << std::endl;
	}
}

void performElementWiseMultiplicationDevice()
{
	int numElements = 1000000;
	thrust::device_vector<double> inputVector1(numElements);

	thrust::device_vector<double> inputVector2(numElements);

	thrust::device_vector<double> outputVector(numElements);

	thrust::fill(inputVector1.begin(), inputVector1.end(), 1.0);

	thrust::fill(inputVector2.begin(), inputVector2.end(), 2.0);


	std::cout << "Size of inputVector " << inputVector1.size() << std::endl;
	std::cout << "Size of inputVector " << inputVector2.size() << std::endl;

	unsigned long long gput = dtime_usec(0);

	thrust::transform(inputVector1.begin(), inputVector1.end(), inputVector2.begin(), outputVector.begin(), ElementWiseMult());

	cudaDeviceSynchronize();
	gput = dtime_usec(gput);
	std::cout << "GPU Timing = " << gput/(float)USECPSEC << " s" << std::endl;
}

void performElementWiseMultiplicationHost()
{
	int numElements = 1000000;
	std::vector<double> inputVector1;
	inputVector1.resize(numElements);

	std::vector<double> inputVector2;
	inputVector2.resize(numElements);

	std::vector<double> outputVector;
	outputVector.resize(numElements);

	fill(outputVector.begin(), outputVector.end(), 0.0);

	double inputRange = (1.0)/ sqrt(10 * 1.0);

	std::uniform_real_distribution<> distrInput(-inputRange, inputRange); // define the range
	std::random_device rdInput; // obtain a random number from hardware
	std::mt19937 engineInput(rdInput()); // seed the generator

	auto generatorInput = std::bind(distrInput, engineInput);
	std::generate_n(inputVector1.begin(), numElements, generatorInput);
	std::generate_n(inputVector2.begin(), numElements, generatorInput);

	std::cout << "Size of inputVector " << inputVector1.size() << std::endl;
	std::cout << "Size of inputVector " << inputVector2.size() << std::endl;

	unsigned long long cput = dtime_usec(0);

	for(int i = 0; i < inputVector1.size(); i++)
	{
	    outputVector[i] = inputVector1[i] * inputVector2[i];
	}
	cput = dtime_usec(cput);
	std::cout << "CPU Timing = " << cput/(float)USECPSEC << " s" <<  std::endl;
}


int main(void)
{
	performElementWiseMultiplicationDevice();

	performElementWiseMultiplicationHost();

	return 0;
}


void matrixMultiplications()
{
	int rowDimension = 3; // number of rows
	int columnDimension = 6; // number of columns

	// initialize data
	thrust::device_vector<double> weightMatrix;
	weightMatrix.resize(rowDimension * columnDimension);

	thrust::device_vector<double> inputVector;
	inputVector.resize(columnDimension);

	thrust::device_vector<double> F;
	F.resize(rowDimension);

	for (size_t i = 0; i < rowDimension; i++)
		for (size_t j = 0; j < columnDimension; j++)
			weightMatrix[j * rowDimension + i]=i;

	for (size_t j = 0; j < columnDimension; j++)
		inputVector[j] = j;

	for (size_t i = 0; i < rowDimension; i++)
		F[i]=0;

	cublasHandle_t handle;

	/* Initialize CUBLAS */
	cublasStatus_t status = cublasCreate(&handle);

	if (status != CUBLAS_STATUS_SUCCESS)
	{
		std::cerr << "!!!! CUBLAS initialization error\n";
	}

	double alpha = 1.0;
	double beta = 0.0;

//	status = cublasDgemv(handle, CUBLAS_OP_N, rowDimension, columnDimension, &alpha, thrust::raw_pointer_cast(weightMatrix.data()), rowDimension,
//			thrust::raw_pointer_cast(inputVector.data()), 1, &beta, thrust::raw_pointer_cast(F.data()), 1) ;;
//
//	if (status != CUBLAS_STATUS_SUCCESS)
//	{
//		std::cerr << "!!!! kernel execution error.\n";
//	}
//
//	for (size_t j = 0; j < rowDimension; j++)
//		std::cout << F[j] << " ";
//
//	std::cout << std::endl;
//
//	weightMatrix.resize(columnDimension);
//	for (size_t j = 0; j < columnDimension; j++)
//		weightMatrix[j]=j;
//
//	thrust::copy(weightMatrix.begin(), weightMatrix.end(), std::ostream_iterator<int>(std::cout, "\t"));
//	std::cout << std::endl;
//	std::cout << std::endl;
//
//	inputVector.resize(rowDimension);
//	for (size_t j = 0; j < rowDimension; j++)
//		inputVector[j]=j;
//
//	thrust::copy(inputVector.begin(), inputVector.end(), std::ostream_iterator<int>(std::cout, "\t"));
//	std::cout << std::endl;
//	std::cout << std::endl;
//
//	F.resize(rowDimension * columnDimension);
//
//	status = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, columnDimension, rowDimension, 1, &alpha, thrust::raw_pointer_cast(weightMatrix.data()), columnDimension,
//			thrust::raw_pointer_cast(inputVector.data()), 1, &beta, thrust::raw_pointer_cast(F.data()), columnDimension);
//
//	status = cublasDestroy(handle);
//	if (status != CUBLAS_STATUS_SUCCESS)
//	{
//		std::cerr << "!!!! shutdown error (A)\n";
//	}
//
//	thrust::copy(F.begin(), F.end(), std::ostream_iterator<int>(std::cout, "\t"));

	int m = 4;
	int n = 3;
	int o = 5;

	weightMatrix.resize(m * n);
	for (size_t j = 0; j < m; j++)
		for (size_t k = 0; k < n; k++)
			weightMatrix[k * m + j]=j;

	thrust::copy(weightMatrix.begin(), weightMatrix.end(), std::ostream_iterator<int>(std::cout, "\t"));
	std::cout << std::endl;
	std::cout << std::endl;

	inputVector.resize(m * o);
	for (size_t j = 0; j < o; j++)
		for (size_t k = 0; k < m; k++)
		inputVector[j * m + k]= j+1;

	thrust::copy(inputVector.begin(), inputVector.end(), std::ostream_iterator<int>(std::cout, "\t"));
	std::cout << std::endl;
	std::cout << std::endl;

	F.resize(n);

	status = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, 1, n, m, &alpha, thrust::raw_pointer_cast(&inputVector[1 * 4]), 1,
			thrust::raw_pointer_cast(weightMatrix.data()), m, &beta, thrust::raw_pointer_cast(F.data()), 1);

//	status = cublasDestroy(handle);
	if (status != CUBLAS_STATUS_SUCCESS)
	{
		std::cerr << "!!!! shutdown error (A)\n";
	}

	thrust::copy(F.begin(), F.end(), std::ostream_iterator<int>(std::cout, "\t"));
	std::cout << std::endl;
	std::cout << std::endl;

//	weightMatrix.resize(m * n);
//	for (size_t j = 0; j < n; j++)
//		for (size_t k = 0; k < m; k++)
//			weightMatrix[j * m + k]=j;

//	thrust::copy(weightMatrix.begin(), weightMatrix.end(), std::ostream_iterator<int>(std::cout, "\t"));

	F.resize(m * m);

	status = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, m, 1, &alpha, thrust::raw_pointer_cast(&weightMatrix[1 * m]), m,
			thrust::raw_pointer_cast(&inputVector[1 * m]), 1, &beta, thrust::raw_pointer_cast(F.data()), m);

	status = cublasDestroy(handle);
	if (status != CUBLAS_STATUS_SUCCESS)
	{
		std::cerr << "!!!! shutdown error (A)\n";
	}

	thrust::copy(F.begin(), F.end(), std::ostream_iterator<int>(std::cout, "\t"));

}
